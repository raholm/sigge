#include <sigge/debug/debug.h>

#include "window.h"

namespace sigge {

  namespace details {

    void key_callback(GLFWwindow* window, int key, int, int action, int)
    {
      if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }

  }  // details

  Window::Window(const unsigned int width,
                 const unsigned int height)
  {
    SIGGE_ASSERT(glfwInit());
    handle = glfwCreateWindow(width, height, "Sigge", nullptr, nullptr);
    SIGGE_ASSERT(handle);
    glfwMakeContextCurrent(handle);
    glewInit();
    glfwSetKeyCallback(handle, details::key_callback);
  }

  Window::~Window()
  {
    glfwDestroyWindow(handle);
    glfwTerminate();
  }

  bool Window::ShouldClose() const
  {
    return glfwWindowShouldClose(handle);
  }

  void Window::SwapBuffers() const
  {
    glfwSwapBuffers(handle);
  }

  void Window::ProcessEvents() const
  {
    glfwPollEvents();
  }

}  // sigge
