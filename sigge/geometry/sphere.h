#pragma once

#include <sigge/math/math.h>
#include <sigge/ray_tracer/material.h>

namespace sigge {

  struct Sphere
  {
    Vector3_F32 center;
    float radius;
    Material material;
  };

}  // sigge
