#pragma once

#include "color.h"

namespace sigge {

  struct Material
  {
    Color color;
    float specularity;
    float reflective;
  };

}  // sigge
