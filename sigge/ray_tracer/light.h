#pragma once

#include <sigge/math/vector3.h>

namespace sigge {

  struct PointLight
  {
    Vector3_F32 position;
    float intensity;
  };

  struct DirectionalLight
  {
    Vector3_F32 direction;
    float intensity;
  };

  struct AmbientLight
  {
    float intensity;
  };

}  // sigge
