#pragma once

#include <iostream>

#define SIGGE_LOG_INFO(message) std::cout << "INFO: " << message << std::endl
#define SIGGE_LOG_ERROR(message) std::cout << "ERROR: " << message << std::endl
