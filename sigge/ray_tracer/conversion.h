#pragma once

#include <sigge/math/vector3.h>

#include "canvas.h"
#include "viewport.h"

namespace sigge {

  Vector3_F32 ConvertCanvasToViewport(const int x,
                                      const int y,
                                      const Canvas& canvas,
                                      const Viewport& viewport);

}  // sigge
