#include <thread>

#include <sigge/debug/debug.h>
#include <sigge/math/math.h>
#include <sigge/ray_tracer/ray_tracer.h>

sigge::Scene SetupScene()
{
  sigge::Scene scene;

  {
    sigge::Sphere sphere;
    sphere.center = { 0.0f, -1.0f, 3.0f };
    sphere.radius = 1.0f;
    sphere.material.color = { 255, 0, 0, 255 };
    sphere.material.specularity = 500.0f;
    sphere.material.reflective = 0.2f;
    scene.spheres.emplace_back(sphere);
  }

  {
    sigge::Sphere sphere;
    sphere.center = { 2.0f, 0.0f, 4.0f };
    sphere.radius = 1.0f;
    sphere.material.color = { 0, 0, 255, 255 };
    sphere.material.specularity = 500.0f;
    sphere.material.reflective = 0.3f;
    scene.spheres.emplace_back(sphere);
  }

  {
    sigge::Sphere sphere;
    sphere.center = { -2.0f, 0.0f, 4.0f };
    sphere.radius = 1.0f;
    sphere.material.color = { 0, 255, 0, 255 };
    sphere.material.specularity = 10.0f;
    sphere.material.reflective = 0.4f;
    scene.spheres.emplace_back(sphere);
  }

  {
    sigge::Sphere sphere;
    sphere.center = { 0.0f, -5001.0f, 0.0f };
    sphere.radius = 5000.0f;
    sphere.material.color = { 255, 255, 0, 255 };
    sphere.material.specularity = 1000.0f;
    sphere.material.reflective = 0.5f;
    scene.spheres.emplace_back(sphere);
  }

  {
    sigge::AmbientLight light;
    light.intensity = 0.2f;
    scene.ambient_light = light;
  }

  {
    sigge::PointLight light;
    light.position = { 2.0f, 1.0f, 0.0f };
    light.intensity = 0.6f;
    scene.point_lights.emplace_back(light);
  }

  {
    sigge::DirectionalLight light;
    light.direction = { 1.0f, 4.0f, 4.0f };
    light.intensity = 0.2f;
    scene.directional_lights.emplace_back(light);
  }

  return scene;
}

void Work(const sigge::Scene& scene,
          const sigge::Viewport& viewport,
          const sigge::Camera& camera,
          const int x_begin,
          const int x_end,
          const int x_offset,
          const int y_begin,
          const int y_end,
          const int y_offset,
          sigge::Canvas& canvas)
{
  for (int x = x_begin; x < x_end; ++x)
  {
    for (int y = y_begin; y < y_end; ++y)
    {
      const sigge::Vector3_F32 viewport_coordinate = sigge::ConvertCanvasToViewport(x, y, canvas, viewport);

      const sigge::Ray3_F32 ray = {
        camera.position,
        (viewport_coordinate - camera.position).Normalized(),
      };

      const sigge::Color color = scene.Trace(ray);

      canvas.Set(x + x_offset, y + y_offset, color);
    }
  }
}

int main()
{
  SIGGE_LOG_INFO("Welcome to Sigge!");

  const unsigned int width = 640;
  const unsigned int height = 640;

  sigge::Canvas canvas(width, height);
  const sigge::Viewport viewport = { 1, 1, 1 };
  const sigge::Camera camera = { 0.0f, 0.0f, 0.0f };

  sigge::Scene scene = SetupScene();

  const int half_width = static_cast<int>(canvas.width) / 2;
  const int half_height = static_cast<int>(canvas.height) / 2;

  std::thread child = std::thread([&]() {
                                    Work(scene,
                                         viewport,
                                         camera,
                                         -half_width,
                                         0,
                                         half_width,
                                         -half_height,
                                         half_height,
                                         half_height,
                                         canvas);
                                  });


  Work(scene,
       viewport,
       camera,
       0,
       half_width,
       half_width,
       -half_height,
       half_height,
       half_height,
       canvas);

  child.join();

  sigge::Viewer(canvas).Show();
  return 0;
}
