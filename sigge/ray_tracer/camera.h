#pragma once

#include <sigge/math/math.h>

namespace sigge {

  struct Camera
  {
    Vector3_F32 position;
  };

}  // sigge
