#pragma once

#include "vector3.h"

namespace sigge {

  struct Ray3_F32
  {
    Vector3_F32 origin;
    Vector3_F32 direction;
  };

}  // sigge
