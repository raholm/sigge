#include <cmath>
#include <limits>

#include <sigge/geometry/intersection.h>
#include <sigge/debug/debug.h>

#include "scene.h"

namespace sigge {

  namespace details {

    constexpr bool IsInInclusiveRange(const float value, const float min, const float max)
    {
      return value >= min && value <= max;
    }

  }  // details

  Color Scene::Trace(const Ray3_F32 ray,
                     const float minimum_t,
                     const float maximum_t,
                     const unsigned int recusion_depth) const
  {
    const Intersection closest_intersection =
      FindClosestIntersection(ray,
                              minimum_t,
                              maximum_t);

    if (!closest_intersection.sphere)
      return {};

    const Vector3_F32 point = ray.origin + closest_intersection.t * ray.direction;
    const Vector3_F32 normal = (point - closest_intersection.sphere->center).Normalized();
    const float specularity = closest_intersection.sphere->material.specularity;
    const Vector3_F32 view_direction = -ray.direction;
    const Color local_color = ComputeLightIntensity(point, normal, view_direction, specularity) * closest_intersection.sphere->material.color;

    if (recusion_depth == 0)
      return local_color;

    const float reflective = closest_intersection.sphere->material.reflective;

    if (reflective <= 0.0f)
      return local_color;

    const Ray3_F32 reflected_ray = {
      point,
      view_direction.ReflectAround(normal).Normalized()
    };

    const Color reflected_color = Trace(reflected_ray, 0.001f, std::numeric_limits<float>::infinity(), recusion_depth - 1);

    return (1.0f - reflective) * local_color + reflective * reflected_color;
  }

  float Scene::ComputeLightIntensity(const Vector3_F32 point,
                                     const Vector3_F32 normal,
                                     const Vector3_F32 view_direction,
                                     const float specularity) const
  {
    const auto ComputeDiffuseIntensityFactor =
      [](const Vector3_F32 light_vector, const Vector3_F32 normal)
        {
          const float angle = normal.Dot(light_vector);

          if (angle <= 0.0f)
            return 0.0f;

          return angle / light_vector.Length();
        };

    const auto ComputeSpecularIntensityFactor =
      [](const Vector3_F32 light_vector,
         const Vector3_F32 normal,
         const Vector3_F32 view_direction,
         const float specularity)
        {
          if (specularity <= 0.0f)
            return 0.0f;

          const Vector3_F32 reflected_direction = light_vector.ReflectAround(normal).Normalized();
          const float cos_angle = reflected_direction.Dot(view_direction);

          if (cos_angle <= 0.0f)
            return 0.0f;

          return std::pow(cos_angle, specularity);
        };

    float result = ambient_light.intensity;

    for (const PointLight& light : point_lights)
    {
      const Vector3_F32 light_vector = light.position - point;

      const Ray3_F32 shadow_ray = {
        point,
        light_vector.Normalized()
      };

      const Intersection shadow = FindClosestIntersection(shadow_ray, 0.001f, light_vector.Length());

      if (shadow.sphere)
        continue;

      result += light.intensity * (ComputeDiffuseIntensityFactor(light_vector, normal) +
                                   ComputeSpecularIntensityFactor(light_vector, normal, view_direction, specularity));
    }

    for (const DirectionalLight& light : directional_lights)
    {
      const Vector3_F32 light_vector = light.direction;

      const Ray3_F32 shadow_ray = {
        point,
        light_vector.Normalized()
      };

      const Intersection shadow = FindClosestIntersection(shadow_ray, 0.001f, std::numeric_limits<float>::infinity());

      if (shadow.sphere)
        continue;

      result += light.intensity * (ComputeDiffuseIntensityFactor(light_vector, normal) +
                                   ComputeSpecularIntensityFactor(light_vector, normal, view_direction, specularity));
    }

    return result;
  }

  Scene::Intersection Scene::FindClosestIntersection(const Ray3_F32 ray,
                                                     const float minimum_t,
                                                     const float maximum_t) const
  {
    float closest_t = std::numeric_limits<float>::infinity();
    const Sphere* closest_sphere = nullptr;

    for (const Sphere& sphere : spheres)
    {
      const SphereRayIntersection intersection = ComputeIntersection(sphere, ray);

      if (details::IsInInclusiveRange(intersection.t1, minimum_t, maximum_t) &&
          intersection.t1 < closest_t)
      {
        closest_t = intersection.t1;
        closest_sphere = &sphere;
      }

      if (details::IsInInclusiveRange(intersection.t2, minimum_t, maximum_t) &&
          intersection.t2 < closest_t)
      {
        closest_t = intersection.t2;
        closest_sphere = &sphere;
      }
    }

    return { closest_t, closest_sphere };
  }

}  // sigge
