#pragma once

namespace sigge {

  struct Vector2_F32
  {
    float x;
    float y;
  };

}  // sigge
