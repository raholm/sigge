#pragma once

namespace sigge {

  struct Viewport
  {
    unsigned int width;
    unsigned int height;
    unsigned int depth;
  };

}  // sigge
