#include "conversion.h"

namespace sigge {

  Vector3_F32 ConvertCanvasToViewport(const int x,
                                      const int y,
                                      const Canvas& canvas,
                                      const Viewport& viewport)
  {
    Vector3_F32 result;
    result.x = static_cast<float>(x) * (static_cast<float>(viewport.width) / static_cast<float>(canvas.width));
    result.y = static_cast<float>(y) * (static_cast<float>(viewport.height) / static_cast<float>(canvas.height));
    result.z = static_cast<float>(viewport.depth);
    return result;
  }

}  // sigge
