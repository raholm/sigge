#!/bin/sh

ROOT_PATH=`dirname $0`
EXECUTABLE=${ROOT_PATH}/build/bin/sigge

if [ ! -f ${EXECUTABLE} ]
then
    echo "Please build first!"
    exit 1
fi

${EXECUTABLE}
