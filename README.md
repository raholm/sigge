# sigge

A very basic CPU-based ray tracer done purely for fun and educational reasons. The final rendered picture:

![Four Spheres](images/four_spheres.png "Four Spheres")
