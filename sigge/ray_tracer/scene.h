#pragma once

#include <vector>
#include <limits>

#include <sigge/geometry/sphere.h>
#include <sigge/math/ray3.h>

#include "color.h"
#include "light.h"

namespace sigge {

  struct Scene
  {
  public:
    Color Trace(const Ray3_F32 ray,
                const float minimum_t = 1.0f,
                const float maximum_t = std::numeric_limits<float>::infinity(),
                const unsigned int recursion_depth = 3) const;

  private:
    struct Intersection
    {
      float t { 0.0f };
      const Sphere* sphere { nullptr };
    };

  private:
    Intersection FindClosestIntersection(const Ray3_F32 ray,
                                         const float minimum_t,
                                         const float maximum_t) const;

    float ComputeLightIntensity(const Vector3_F32 point,
                                const Vector3_F32 normal,
                                const Vector3_F32 view_direction,
                                const float specularity) const;

  public:
    std::vector<Sphere> spheres;
    std::vector<PointLight> point_lights;
    std::vector<DirectionalLight> directional_lights;
    AmbientLight ambient_light;

  };

}  // sigge
