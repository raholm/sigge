#include <cmath>
#include <limits>

#include <sigge/debug/debug.h>

#include "intersection.h"

namespace sigge {

  SphereRayIntersection ComputeIntersection(const Sphere sphere, const Ray3_F32 ray)
  {
    const Vector3_F32 sphere_to_ray = ray.origin - sphere.center;

    const float p = ray.direction.Dot(sphere_to_ray);
    const float q = sphere_to_ray.Dot(sphere_to_ray) - (sphere.radius * sphere.radius);

    const float disciminant = (p * p) - q;

    if (disciminant < 0.0f)
      return { std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity() };

    const float delta = std::sqrt(disciminant);
    return { -p - delta, -p + delta };
  }

}  // sigge
