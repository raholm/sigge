#pragma once

#include "sphere.h"

namespace sigge {

  struct SphereRayIntersection
  {
    float t1;
    float t2;
  };

  SphereRayIntersection ComputeIntersection(const Sphere sphere, const Ray3_F32 ray);

}  // sigge
