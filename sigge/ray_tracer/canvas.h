#pragma once

#include <vector>

#include "color.h"

namespace sigge {

  struct Canvas
  {
  public:
    Canvas(const unsigned int width,
           const unsigned int height);

    void Set(const unsigned int x,
             const unsigned int y,
             const Color color);

    Color Get(const unsigned int x,
              const unsigned int y) const;

  public:
    unsigned int width;
    unsigned int height;
    std::vector<Color> pixels;
  };

}  // sigge
