#include <sigge/debug/debug.h>

#include "canvas.h"

namespace sigge {

  Canvas::Canvas(const unsigned int width,
                 const unsigned int height)
    : width(width),
      height(height)
  {
    pixels.resize(width * height);

    for (Color& pixel : pixels)
    {
      pixel.red = 0;
      pixel.green = 0;
      pixel.blue = 0;
      pixel.alpha = 255;
    }
  }

  void Canvas::Set(const unsigned int x,
                   const unsigned int y,
                   const Color color)
  {
    SIGGE_ASSERT(x < width);
    SIGGE_ASSERT(y < height);

    pixels[x + y * width] = color;
  }

  Color Canvas::Get(const unsigned int x,
                    const unsigned int y) const
  {
    SIGGE_ASSERT(x < width);
    SIGGE_ASSERT(y < height);

    return pixels[x + y * width];
  }

}  // sigge
