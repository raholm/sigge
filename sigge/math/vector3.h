#pragma once

namespace sigge {

  struct Vector3_F32
  {
  public:
    Vector3_F32 operator-(const Vector3_F32 rhs) const;
    Vector3_F32& operator-=(const Vector3_F32 rhs);

    Vector3_F32 operator+(const Vector3_F32 rhs) const;
    Vector3_F32& operator+=(const Vector3_F32 rhs);

    Vector3_F32 operator*(const float rhs) const;
    Vector3_F32& operator*=(const float rhs);

    Vector3_F32 operator-() const;

    float LengthSquared() const;
    float Length() const;

    Vector3_F32 Normalized() const;
    Vector3_F32 ReflectAround(const Vector3_F32 axis) const;

    float Dot(const Vector3_F32 rhs) const;

  public:
    float x;
    float y;
    float z;
  };

  Vector3_F32 operator*(const float lhs, const Vector3_F32 rhs);

}  // sigge
