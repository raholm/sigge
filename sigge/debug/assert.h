#pragma once

#include <cassert>

#define SIGGE_ASSERT(expression) assert(expression)
